/**
 * @file proj1.c
 * @brief Implementacao do primeiro projecto de IAED 2015/2016.
 * @author Pedro Caetano n. 56564
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define MAX_COD_AERO 4
#define MAX_AEROS 1000
#define MAX_VOOS_ARRAY 1001 /*MaxVoos=1000 logo o array tera tamanho 1001*/
#define INEXISTENTE -1 /*valor de retorno para aeroporto nao encontrado*/

/**
 * Estrutura para representar um aeroporto
 */
 
typedef struct aeroporto{ 
	char codigo[MAX_COD_AERO]; /* codigo do aeroporto */
	int cap; /* capacidade do aeroporto */
	int estado; /*estado 1=aberto 0=fechado */
} aeroporto;

/**
 * Variaveis a usar no programa.
 */
static aeroporto rede[MAX_AEROS];
static int ligacoes[MAX_AEROS][MAX_AEROS];
static int n=0;

/**
 * Prototipos das funcoes do program
 */
void Commando_A();
void Commando_I();
void Commando_F();
void Commando_G();
void Commando_R();
void Commando_S();
void Commando_N();
void Commando_P();
void Commando_Q();
void Commando_V();
void Commando_C();
void Commando_O();

void Commando_L();
void Commando_L_0();
void Commando_L_1();
void Commando_L_2();

void Commando_X();

void mudaVoos(int ida, int volta, char c1[MAX_COD_AERO], char c2[MAX_COD_AERO]);
void insertion( int id[],int l, int r);
int voosIda(int id);
int voosVolta(int id);
int totalVoosAero (int id);
int totalVoos();
int idAeroporto(char codigo[]);
int nLigacoes (int id);
aeroporto novoAeroporto(char codigo[],int cap);

/**
 * @brief Funcao main.
 *
 * @return Codigo de estado de saida do programa.
 */
int main (){
	char command;
	while(1){
		command=getchar();
		switch (command){
			case 'A':
				Commando_A();
				break;
			case 'I':
				Commando_I();
				break;
			case 'F':
				Commando_F();
				break;
			case 'G':
				Commando_G();
				break;
			case 'R':
				Commando_R();
				break;
			case 'S':
				Commando_S();
				break;
			case 'N':
				Commando_N();
				break;
			case 'P':
				Commando_P();
				break;
			case 'Q':
				Commando_Q();
				break;
			case 'V':
				Commando_V();
				break;
			case 'C':
				Commando_C();
				break;
			case 'O':
				Commando_O();
				break;
			case 'L':
				Commando_L();
				break;
			case 'X':
				Commando_X();
				return EXIT_SUCCESS; /*Termina o Programa*/
			default:
				;/*nao deve chegar aqui*/ 
		}
	}
	return EXIT_FAILURE;
}

/**
 * @brief funcao para processar o comando 'A'
 */
void Commando_A(){
	int i,cap;
	char codigo[MAX_COD_AERO];
	scanf("%s%d",codigo,&cap);
	getchar(); /*apanha o caracter de fim de linha*/
	rede[n] = novoAeroporto(codigo,cap);
	
	for(i=0;i<=n;i++) /*inicializa a matriz ligacoes*/
	{
		ligacoes[n][i]=0;
		ligacoes[i][n]=0;
	}
	n++;
}

/**
 * @brief funcao para processar o comando 'I'
 */
void Commando_I(){
	char codigo[MAX_COD_AERO];
	int nCap, id;
	scanf("%s%d",codigo,&nCap);
	getchar(); /*apanha o caracter de fim de linha*/
	id=idAeroporto(codigo);
	
	if (id!=INEXISTENTE && rede[id].estado==1 && (nCap+=rede[id].cap)>=totalVoosAero(id))
		rede[id].cap=nCap;
	else
		printf("*Capacidade de %s inalterada\n",codigo);
}

/**
 * @brief funcao para processar o comando 'F'
 */
void Commando_F(){	
	char c1[MAX_COD_AERO],c2[MAX_COD_AERO];
	scanf("%s%s",c1,c2);
	getchar(); /*apanha o caracter de fim de linha*/
	mudaVoos(1,1,c1,c2);
}

/**
 * @brief funcao para processar o comando 'G'
 */
void Commando_G(){	
	char c1[MAX_COD_AERO],c2[MAX_COD_AERO];
	scanf("%s%s",c1,c2);
	getchar(); /*apanha o caracter de fim de linha*/
	mudaVoos(1,0,c1,c2);
}

/**
 * @brief funcao para processar o comando 'R'
 */
void Commando_R(){	
	char c1[MAX_COD_AERO],c2[MAX_COD_AERO];
	scanf("%s%s",c1,c2);
	getchar(); /*apanha o caracter de fim de linha*/
	mudaVoos(-1,0,c1,c2);
}

/**
 * @brief funcao para processar o comando 'S'
 */
void Commando_S(){	
	char c1[MAX_COD_AERO],c2[MAX_COD_AERO];
	scanf("%s%s",c1,c2);
	getchar(); /*apanha o caracter de fim de linha*/
	mudaVoos(-1,-1,c1,c2);
}

/**
 * @brief funcao para processar o comando 'N'
 */
void Commando_N(){
	char c1[MAX_COD_AERO],c2[MAX_COD_AERO];
	int id1,id2;
	scanf("%s%s",c1,c2);
	getchar(); /*apanha o caracter de fim de linha*/
	id1=idAeroporto(c1);
	id2=idAeroporto(c2);
	if (id1==INEXISTENTE)
		printf("Aeroporto %s inexistente\n",c1);
	if (id2==INEXISTENTE)
		printf("Aeroporto %s inexistente\n",c2);
	if(id1!=INEXISTENTE && id2!=INEXISTENTE)
		printf("Voos entre cidades %s:%s:%d:%d\n",c1,c2,ligacoes[id1][id2],ligacoes[id2][id1]);
}

/**
 * @brief funcao para processar o comando 'P'
 */
void Commando_P(){
	int i,id=0,nVoos,nVoosAux;
	getchar(); /*apanha o caracter de fim de linha*/
	nVoos=totalVoosAero(id);
	for(i=0;i<n;i++){
			if((nVoosAux=totalVoosAero(i))>nVoos){
					id=i;
					nVoos=nVoosAux;
				}
		}
	printf("Aeroporto com mais rotas %s:%d:%d\n",rede[id].codigo,voosIda(id),voosVolta(id));
}

/**
 * @brief funcao para processar o comando 'Q'
 */
void Commando_Q(){
	int i,id=0,nlig,nligAux;
	getchar(); /*apanha o caracter de fim de linha*/
	nlig=nLigacoes(id);
	for(i=0;i<n;i++){
		if((nligAux=nLigacoes(i))>nlig){
			id=i;
			nlig=nligAux;
		}
	}
	printf("Aeroporto com mais ligacoes %s:%d\n",rede[id].codigo,nlig);
}

/**
 * @brief funcao para processar o comando 'V'
 */
void Commando_V(){
	int i,j,id1=0,id2=0,nVoos=0;
	getchar(); /*apanha o caracter de fim de linha*/
	for(i=0;i<n;i++){
		for(j=0;j<=n;j++){
			if(ligacoes[i][j]>nVoos){
				nVoos=ligacoes[i][j];
				id1=i;
				id2=j;
			}
		}
	}
	printf("Voo mais popular %s:%s:%d\n",rede[id1].codigo,rede[id2].codigo,nVoos);
}

/**
 * @brief funcao para processar o comando 'C'
 */
void Commando_C(){
	int id,i;
	char c[MAX_COD_AERO];
	scanf("%s",c);
		getchar(); /*apanha o caracter de fim de linha*/
	id=idAeroporto(c);
	if(id!=INEXISTENTE){
		rede[id].estado=0;
		for(i=0;i<n;i++){
			ligacoes[id][i]=0;
			ligacoes[i][id]=0;
		}
	}
	else
		printf("*Aeroporto %s inexistente\n",c);
}

/**
 * @brief funcao para processar o comando 'O'
 */
void Commando_O(){
	int id;
	char c[MAX_COD_AERO];
	scanf("%s",c);
	getchar(); /*apanha o caracter de fim de linha*/
	id=idAeroporto(c);
	if(id!=INEXISTENTE)
		rede[id].estado=1;
	else
		printf("*Aeroporto %s inexistente\n",c);
}

/**
 * @brief funcao para processar o comando 'L'
 */
void Commando_L(){
	char subcommand;
	getchar(); //Capture space
	subcommand=getchar();
	getchar(); /*apanha o caracter de fim de linha*/
	switch(subcommand){
		case '0':
			Commando_L_0();
			break;
		case '1':
			Commando_L_1();
			break;
		case '2':
			Commando_L_2();
			break;
		default:
			;/*nao deve chegar aqui*/
	}
}	

/**
 * @brief funcao para processar o subcomando 'L 0'
 */
void Commando_L_0(){
	int i;
	for(i=0;i<n;i++)
		printf("%s:%d:%d:%d\n",rede[i].codigo,rede[i].cap,voosIda(i),voosVolta(i));
}

/**
 * @brief funcao para processar o subcomando 'L 1'
 */
void Commando_L_1(){
	int i, id[MAX_AEROS];
	
	/*preenche vector auxiliar a ordenar*/
	for (i=0;i<n;i++)
		id[i]=i;
	insertion(id,0,n-1);
	for(i=0;i<n;i++)
		printf("%s:%d:%d:%d\n",rede[id[i]].codigo,rede[id[i]].cap,voosIda(id[i]),voosVolta(id[i]));
}

/**
 * @brief funcao para processar o subcomando 'L 2'
 */
void Commando_L_2(){
	int hist[MAX_VOOS_ARRAY],i;
	
	for(i=0;i<MAX_VOOS_ARRAY;i++)
		hist[i]=0;
	for(i=0;i<n;i++)
		hist [totalVoosAero(i)]++;
	for(i=0;i<MAX_VOOS_ARRAY;i++)
		if (hist[i]>0)
			printf("%d:%d\n",i,hist[i]);
}

/**
 * @brief funcao para processar o comando 'X'
 */
void Commando_X(){
	printf("%d:%d\n",totalVoos(),n);
}

/**
 * @brief funcao para adicionar/remover voos entre dois aeroportos
 * 
 * @param 'ida' numero de voos de ida a adicionar (negativo para remover),
 * 'volta' numero de voos de volta a adicionar (negativo para remover),
 * 'c1' codigo do aeroporto de origem
 * 'c2' codigo do aeroporto de destino
 */
void mudaVoos(int ida, int volta, char c1[MAX_COD_AERO], char c2[MAX_COD_AERO]){
	char addStr[10],rtStr[4]; /* 10=len("adicionar")+1 4=len("RT ")*/
	int tot1,tot2,id1,id2;
	id1=idAeroporto(c1);
	id2=idAeroporto(c2);
	strncpy(addStr,(ida>0)?"adicionar":"remover",10); /*string para msg erro*/
	strncpy(rtStr,(ida==volta)?"RT ":"",4); /*str para msg erro inclui espaco*/
	if (id1!=INEXISTENTE && id2!=INEXISTENTE && rede[id1].estado==1 && rede[id2].estado==1){
		tot1=totalVoosAero(id1)+ida+volta;
		tot2=totalVoosAero(id2)+ida+volta;
		if(tot1<=rede[id1].cap && tot2<=rede[id2].cap 
			&& (ligacoes[id1][id2]+ida)>=0 && (ligacoes[id2][id1]+volta)>=0){
			ligacoes[id1][id2]+=ida; // adiciona/remove um voo de c1 para c2
			ligacoes[id2][id1]+=volta; // adiciona/remove um voo de c2 para c1
		}
		else
			printf("*Impossivel %s voo %s%s %s\n",addStr,rtStr,c1,c2);
	}
	else
		printf("*Impossivel %s voo %s%s %s\n",addStr,rtStr,c1,c2);
}

/**
 * @brief funcao para calcular os voos de ida
 * 
 * @param 'id' identificador do aeroporto
 * 
 * @return numero de voos a sair do aeroporto com o identificador id
 */
int voosIda(int id){
	int i,total=0;
	for (i=0;i<n;i++)
		total+=ligacoes[id][i];
	return total;
}

/**
 * @brief funcao para calcular os voos de volta
 * 
 * @param 'id' identificador do aeroporto
 * 
 * @return numero de voos a chegar do aeroporto com o identificador id
 */
int voosVolta(int id){
	int i,total=0;
	for (i=0;i<n;i++)
		total+=ligacoes[i][id];
	return total;
}

/**
 * @brief funcao para calcular o numero total de voos
 * 
 * @param 'id' identificador do aeroporto
 * 
 * @return numero total de voos a chegar ou partir do aeroporto com 
 * o identificador id
 */
int totalVoosAero (int id){
	return voosIda(id) + voosVolta(id);
}

/**
 * @brief funcao para calcular o numero de ligacoes de um aeroporto
 * 
 * @param 'id' identificador do aeroporto
 * 
 * @return numero total de ligacoes ao aeroporto com o identificador id
 */
int nLigacoes (int id){
	int i,nlig=0;
	for(i=0;i<n;i++)
		if(ligacoes[id][i]>0 || ligacoes[i][id]>0)
			nlig++;
	return nlig;
}

/**
 * @brief funcao para calcular o numero total voos da rede de aeroportos
 */
int totalVoos(){
	int total=0,i,j;
	for(i=0;i<n;i++)
		for(j=0;j<=n;j++)
			total+=ligacoes[i][j];
	return total;
}

/**
 * @brief funcao para ordenar o id dos aeroportos de forma lexicografica
 * 
 * @param 'id' array com o identificador dos aeroportos, 'l' identificador
 * do primeiro aeroporto, 'r' inditificador do ultimo aeroporto
 */
void insertion(int id[],int l, int r){  
	int i,j,x; 
	for (i = l+1; i <= r; i++) { 
		x=id[i];		
		j = i-1; 
		while (j >= l && strcmp(rede[x].codigo,rede[id[j]].codigo)<0) { 
			id[j+1] = id[j]; 
			j--; 
		} 
		id[j+1] = x; 
	}
}

/**
 * @brief funcao para criar um novo aeroporto
 * 
 * @param 'codigo' codigo de 3 caracteres do novo aeroporto, 
 * 'cap' capacidade do novo aeroporto
 * 
 * @return estrutura do aeroporto com 'codido' 'cap' e com estado=1
 */
aeroporto novoAeroporto(char codigo[],int cap){
	aeroporto ae;
	strncpy(ae.codigo,codigo,MAX_COD_AERO);
	ae.cap = cap;
	ae.estado = 1;
	return ae;
}

/**
 * @brief funcao para encontrar o identificador do aeroporto
 * 
 * @param 'codigo' codigo de 3 caracteres do aeroporto
 * 
 * @return identificador do aeroporto
 */
int idAeroporto(char codigo[]){
	int i;
	for(i=0;i<n && strcmp(rede[i].codigo,codigo)!=0;i++);
	if (i==n) // se i for igual a n o aeroporto nao foi encontrado
		return INEXISTENTE;
	else
		return i;
}
